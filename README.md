#Shutdown

#简介
一键重启,主要是为方便重启，点击一键重启图标，即重启手机。首次使用，需要授予root权限。

#演示
http://www.wandoujia.com/apps/com.mj.shutdown

#捐赠
开源，我们是认真的，感谢您对我们开源力量的鼓励。


![支付宝](https://git.oschina.net/uploads/images/2017/0607/164544_ef822cc0_395618.png "感谢您对我们开源力量的鼓励")
![微信](https://git.oschina.net/uploads/images/2017/0607/164843_878b9b7f_395618.png "感谢您对我们开源力量的鼓励")

