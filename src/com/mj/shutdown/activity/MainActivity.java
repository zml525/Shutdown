package com.mj.shutdown.activity;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.shutdown.R;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
/**
 * 一键关机
 * TODO 需要root权限 并且联想P700i关机后按开机键启动不起来 需要拔掉电池重装后才行
 * @author zhaominglei
 * @date 2015-2-3
 * 
 */
public class MainActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = MainActivity.class.getSimpleName();
	private boolean isExit = false;
	private TimerTask timerTask;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private Button shutdownBtn; //关机
	private Button rebootBtn; //重启
	private ImageView rebootView;
	
//	private Intent intent;
	private Button appOffersButton; //推荐应用
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		init();
	}
	
	private void init() {
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.miniAdLinearLayout);
		shutdownBtn = (Button)findViewById(R.id.shutdown_shutdown);
		rebootBtn = (Button)findViewById(R.id.shutdown_reboot);
		rebootView = (ImageView)findViewById(R.id.shutdown_reboot_imageview);
		appOffersButton = (Button)findViewById(R.id.appOffersButton);
		
		shutdownBtn.setOnClickListener(this);
		rebootBtn.setOnClickListener(this);
		rebootView.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);

		appControl = AppControl.getInstance();
		appControl.init(MainActivity.this, "663826883de796aa/hen5ORyovNx/krOJ4dgQ5Vj72Wu41+uxCSLrRmdVfmefSeTcg", "木蚂蚁");
		appControl.loadPopAd(MainActivity.this);
		appControl.showPopAd(MainActivity.this, 60 * 1000);
		appControl.showInter(MainActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(MainActivity.this, "663826883de796aa/hen5ORyovNx/krOJ4dgQ5Vj72Wu41+uxCSLrRmdVfmefSeTcg", "木蚂蚁");
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.shutdown_shutdown:
//			intent = new Intent(Intent.ACTION_SHUTDOWN);
////			intent.putExtra(Intent.EXTRA_KEY_CONFIRM, false);
//			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			startActivity(intent);
			shutdown();
			break;
		
		case R.id.shutdown_reboot:
//			intent = new Intent(Intent.ACTION_REBOOT);
//			intent.putExtra("nowait", 1);
//			intent.putExtra("interval", 1);
//			intent.putExtra("window", 0);
//			sendBroadcast(intent);
			reboot();
			break;
			
		case R.id.shutdown_reboot_imageview:
			reboot();
			break;
		
		case R.id.appOffersButton:
			access.openWALL(MainActivity.this);
			break;
			
		default:
			break;
		}
	}

	private void shutdown() {
		String cmd = "su -c shutdown -h";
		exeCmd(cmd);
	}

	private void reboot() {
		String cmd = "su -c reboot";
		exeCmd(cmd);
	}
	
	private void exeCmd(String cmd) {
		try {
			Runtime.getRuntime().exec(cmd);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isExit) {
			MainActivity.this.finish();
		} else {
			isExit = true;
			Toast.makeText(MainActivity.this, R.string.exit_msg, Toast.LENGTH_SHORT).show();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			};
			new Timer().schedule(timerTask, 2*1000);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}
}
